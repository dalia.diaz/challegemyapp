import React from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import Home from "../containers/Home";
import NotFound from "../containers/NotFound";
import Productos from "../containers/Productos";
import MyCart from "../containers/MyCart";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/list-productos" component={Productos} exact />
          <Route path="/my-cart" component={MyCart} exact />

          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
