export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";

export function addCart(info) {
  return (dispatch) => {
    dispatch({
      type: INCREMENT,
      payload: info,
    });
  };
}

export function DeleteCart(id, index) {
  return (dispatch) => {
    dispatch({
      type: DECREMENT,
      idDeleteMyCart: id,
      indexDeleteCart: index,
    });
  };
}
