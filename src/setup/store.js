import { createStore, combineReducers, applyMiddleware, compose } from "redux";
//import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from "redux-thunk";
import * as productMyCart from "./reducer/state";

// Root Reducer, needs a previous state and action
const rootReducer = combineReducers({
  ...productMyCart,
});

// Store
export const store = createStore(
  rootReducer,
  //initial state. thunk:async functions
  compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : (f) => f
  )
);
