import { INCREMENT, DECREMENT } from "../actions/actions";
import { jsonData } from "../../constants/constants";

const getData = {
  info: 0,
  error: null,
  isLoading: false,
  dataMyCart: [],
  dataProductos: jsonData.products,
};
export const productMyCart = (state = getData, action) => {
  switch (action.type) {
    case INCREMENT:
      const myCartData = state.dataProductos.map((res) => {
        if (res.id === action.payload.id) {
          const value = res.amount - 1;
          res.amount = value;
        }
        return res;
      });
      return {
        ...state,
        info: state.info + 1,
        dataMyCart: [action.payload, ...state.dataMyCart],
        dataProductos: myCartData,
      };
    case DECREMENT:
      const dataFilter = state.dataMyCart.filter(
        (res, index) => index !== action.indexDeleteCart
      );
      const myData = state.dataProductos.map((res) => {
        if (res.id === action.idDeleteMyCart) {
          const value = res.amount + 1;
          res.amount = value;
        }
        return res;
      });
      return {
        ...state,
        info: state.info - 1,
        dataMyCart: dataFilter,
        dataProductos: myData,
      };
    default:
      return state;
  }
};
