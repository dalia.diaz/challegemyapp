import React from "react";
import Header from "../components/Header/Header";
import Container from "react-bootstrap/Container";

import { Col, Table, Button } from "react-bootstrap";
import Card from "../components/Card/Card";
import { connect } from "react-redux";
import { DeleteCart } from "../setup/actions/actions";

const MyCart = (props) => {
  const { myCart } = props;
  const removeToCart = (id, index) => {
    props.DeleteCart(id, index);
  };

  const getTotal = () => {
    if (myCart.length > 0) {
      let initialValue = 0;
      let sum = myCart.reduce(function (accumulator, curValue) {
        return accumulator + curValue.price;
      }, initialValue);
      return sum;
    }
    return 0;
  };
  return (
    <div
      className="content"
      style={{ textAlign: "center", alignItems: "center" }}
    >
      <Header myCart={myCart.length} />
      <Container fluid>
        <Col md={12}>
          <Card
            title="My Cart"
            category=""
            ctTableFullWidth
            ctTableResponsive
            content={
              <div className="row">
                <div
                  className="col-sm-12"
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  {myCart.length > 0 ? (
                    <Table striped hover bordered responsive>
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Price</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      {myCart.map((res, index) => (
                        <tbody key={index}>
                          <tr>
                            <td>{res.name}</td>
                            <td>{res.price}</td>
                            <td>{res.amount}</td>
                            <td>
                              <Button
                                variant="outline-success"
                                active
                                size="lg"
                                onClick={() => removeToCart(res.id, index)}
                              >
                                Remove to cart
                              </Button>
                            </td>
                          </tr>
                        </tbody>
                      ))}
                    </Table>
                  ) : (
                    <div>
                      <br />
                      <h1>No Items</h1>
                      <hr />
                    </div>
                  )}
                </div>
                <div className="col-sm-12">
                  <p>TOTAL: {getTotal()} </p>
                </div>
              </div>
            }
          />
        </Col>
      </Container>
    </div>
  );
};
const mapStateToProps = (state) => ({
  myCart: state.productMyCart.dataMyCart,
});

export default connect(mapStateToProps, { DeleteCart })(MyCart);
