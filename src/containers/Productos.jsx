import React from "react";
import { Col, Table, Button } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Card from "../components/Card/Card";
import Header from "../components/Header/Header";
import { connect } from "react-redux";
import { addCart, DeleteCart } from "../setup/actions/actions";
const Productos = (props) => {
  const { myCart, dataProductos } = props;
  const addToCart = (info) => {
    props.addCart(info);
  };
  return (
    <div
      className="content"
      style={{ textAlign: "center", alignItems: "center" }}
    >
      <Header myCart={myCart.length} />
      <Container fluid>
        <Col md={12}>
          <Card
            title="Ocupacion de Patios"
            category=""
            ctTableFullWidth
            ctTableResponsive
            content={
              <div className="row">
                <div className="col-sm-12">
                  <p>LIST PRODUCTS</p>
                </div>
                <div
                  className="col-sm-12"
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <Table striped hover bordered responsive>
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    {dataProductos.map((res) => (
                      <tbody key={res.id}>
                        <tr>
                          <td>{res.name}</td>
                          <td>{res.price}</td>
                          <td>{res.amount}</td>
                          <td>
                            <Button
                              variant="outline-success"
                              active
                              size="lg"
                              onClick={() => addToCart(res)}
                              disabled={res.amount === 0 ? true : false}
                            >
                              Add to cart
                            </Button>
                          </td>
                        </tr>
                      </tbody>
                    ))}
                  </Table>
                </div>
              </div>
            }
          />
        </Col>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  myCart: state.productMyCart.dataMyCart,
  dataProductos: state.productMyCart.dataProductos,
});

export default connect(mapStateToProps, { addCart, DeleteCart })(Productos);
